import pickle

DETS = 5

with open('result_original.pkl', 'rb') as f:
    x = pickle.load(f)

x = x[:DETS]

with open('result_reduced.pkl', 'wb') as f:
    pickle.dump(x, f)
