import os

import kitti_common as kitti_utils
from calibration_kitti import Calibration

import numpy as np

import pickle

def load_pc(filename, intensity=True):
    try:
        ret = np.fromfile(filename, dtype=np.float32).reshape(-1, 4)
        if not intensity:
            ret = ret[:,:3]
    except Exception:
        ret = None
    return ret

def get_idxes(idx_list_file):
    idxes = []
    with open(idx_list_file, 'rt') as f:
        idxes = [ l.strip() for l in f if l ]
    return idxes

class KittiData:
    def __init__(self, base_path, dets_path, idx_txt, velo_reduced=True, frames=None):
        self.data_path = os.path.join(base_path, 'training')
        self.calib_path = os.path.join(base_path, 'training/calib')
        self.frames = frames
        self.velo_prefix = 'velodyne'
        self.anno_path = os.path.join(base_path, 'training/label_2')
        if velo_reduced:
            self.velo_prefix += '_reduced'

        self.idxes = get_idxes(idx_txt)
        self._get_file_lists()

        self._load_dets(dets_path)
        self._load_calib()

    def _process_annos(self, annotations, calib, classes, num_objects=None):
        annotations = kitti_utils.filter_kitti_anno(
            annotations,
            used_classes = classes,
        )
        loc = annotations['location']
        dims = annotations['dimensions']
        rots = annotations['rotation_y']
        if num_objects is not None:
            loc = loc[:num_objects]
            dims = dims[:num_objects]
            rots = rots[:num_objects]
        loc_lidar = calib.rect_to_lidar(loc)
        l, h, w = dims[:, 0:1], dims[:, 1:2], dims[:, 2:3]
        loc_lidar[:, 2] += h[:, 0] / 2
        gt_boxes_lidar = np.concatenate([loc_lidar, l, w, h, -(np.pi / 2 + rots[..., np.newaxis])], axis=1)
        annotations['gt_boxes_lidar'] = gt_boxes_lidar
        annotations['corners3d'] = kitti_utils.boxes_to_corners_3d(gt_boxes_lidar)
        return annotations

    def get_velo(self, idx):
        try:
            filename = self.velo_files[idx]
            pc = load_pc(filename)
        except Exception:
            pc = None
        return pc

    def get_dets(self, idx, classes, thresh, num_objects=None):
        try:
            dets = self.dets[idx]
            dets = kitti_utils.filter_annos_low_score([dets], thresh)[0]
            calib = self.calibs[idx]
            ret = self._process_annos(dets, calib, classes, num_objects)
        except (IndexError, KeyError):
            ret = None
        except Exception:
            ret = None
        return ret  

    def get_anno(self, idx, classes, num_objects=None):
        try:   
            annotations = kitti_utils.get_label_anno(self.anno_files[idx])
            calib = self.calibs[idx]
            ret = self._process_annos(annotations, calib, classes, num_objects)
        except (IndexError, KeyError):
            ret = None
        except Exception:
            ret = None
        return ret

    def _load_dets(self, det_filename):
        with open(det_filename, 'rb') as f:
            self.dets = pickle.load(f)
        if self.frames is not None:
            self.dets = [ self.dets[i] for i in self.frames ]
        self.dets = { self.idxes[i] : det for i, det in enumerate(self.dets) }

    def _load_calib(self):
        self.calibs = {}
        for idx, filename in self.calib_files.items():
            self.calibs[idx] = Calibration(filename)

    def _get_file_lists(self):
        """Find and list data files for each sensor."""

        # Subselect the chosen range of frames, if any
        if self.frames is not None:
            self.idxes = [ self.idxes[i] for i in self.frames ]
        
        self.cam2_files = {
            idx : os.path.join(self.data_path, 'image_02', f'{idx}.png')
            for idx in self.idxes
        }
        self.velo_files = {
            idx : os.path.join(self.data_path, self.velo_prefix, f'{idx}.bin')
            for idx in self.idxes
        }
        self.calib_files = {
            idx : os.path.join(self.calib_path, f'{idx}.txt')
            for idx in self.idxes
        }
        self.anno_files = {
            idx :os.path.join(self.anno_path, f'{idx}.txt')
            for idx in self.idxes
        }
