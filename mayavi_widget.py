import os
os.environ['ETS_TOOLKIT'] = 'qt'
os.environ['QT_API'] = 'pyqt'

from PyQt5.QtWidgets import QWidget, QVBoxLayout

from traits.api import HasTraits, Instance, on_trait_change
from traitsui.api import View, Item
from mayavi.core.ui.api import MayaviScene, MlabSceneModel, \
        SceneEditor

import numpy as np

from vis_utils import visualize_pts, draw_corners3d

################################################################################
#The actual visualization
class Visualization(HasTraits):

    scene = Instance(MlabSceneModel, ())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fig = None

    def _set_bg(self, color=(1.0,)*3):
        if self.fig is not None:
            self.fig.scene.background = color

    def plot_pc(self, pc, clear=True):
        if clear:
            self.scene.mlab.clf(self.fig)
        self._set_bg()
        visualize_pts(self.scene.mlab, pc, fig=self.fig, show_intensity=True)

    def plot_corners(self, corners3d, **kwargs):
        draw_corners3d(self.scene.mlab, corners3d=corners3d, fig=self.fig, **kwargs)

    @on_trait_change('scene.activated')
    def update_plot(self):
        if self.fig is None:
            self.fig = self.scene.mlab.gcf()
        self._set_bg()
        self.scene.mlab.clf(self.fig)

    # the layout of the dialog screated
    view = View(Item('scene', editor=SceneEditor(scene_class=MayaviScene),
                     height=250, width=300, show_label=False),
                resizable=True # We need this to resize with the parent widget
                )


class MayaviQWidget(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        layout = QVBoxLayout(self)
        layout.setContentsMargins(0,0,0,0)
        layout.setSpacing(0)
        self.visualization = Visualization()

        # The edit_traits call will generate the widget to embed.
        self.ui = self.visualization.edit_traits(parent=self,
                                                 kind='subpanel').control
        layout.addWidget(self.ui)
        self.ui.setParent(self)
