from PyQt5 import uic
from PyQt5.QtWidgets import QApplication

import kitti

ANNO_COLOR = (1, 0, 0) #red
DET_COLOR  = (0, 1, 0) #green
KITTI_PATH = './data'
IDX_TXT = './data/val.txt'
DETS_PATH = './data/result.pkl'
WINDOW_QDESIGNER_FILE = 'window0.ui'

cls, wnd = uic.loadUiType(WINDOW_QDESIGNER_FILE)

class LidarDetWindow(wnd, cls):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.kitti_data = kitti.KittiData(
            base_path    = KITTI_PATH, 
            velo_reduced = False,
            idx_txt      = IDX_TXT,
            dets_path    = DETS_PATH,
        )
        self.cbChoosePc.addItems(self.kitti_data.idxes)
        self.pc_idx = None
        self.pc     = None

    def get_classes(self):
        classes = []
        if self.checkCar.checkState() > 0:
            classes.append('Car')
        if self.checkCyclist.checkState() > 0:
            classes.append('Cyclist')
        if self.checkPedestrian.checkState() > 0:
            classes.append('Pedestrian')
        return classes

    def get_pc(self):
        self.pc_idx = self.cbChoosePc.currentText()
        self.pc = self.kitti_data.get_velo(self.pc_idx)

    def reload_annotations(self):
        thresh = self.sbScoreThresh.value()
        classes = self.get_classes()
        try:
            anno = self.kitti_data.get_anno(self.pc_idx, classes)
            det = self.kitti_data.get_dets(self.pc_idx, classes, thresh=thresh)
        except IndexError:
            print(f'Point cloud {self.pc_idx}.bin does not exist')
        else:
            if self.pc is not None:
                self.pcShow.visualization.plot_pc(self.pc)
                if anno is not None:
                    self.pcShow.visualization.plot_corners(anno['corners3d'], color=ANNO_COLOR)
                if det is not None:
                    self.pcShow.visualization.plot_corners(det['corners3d'], color=DET_COLOR)

    def on_pbPcLoad_released(self):
        self.get_pc()
        self.reload_annotations()

    def on_pbReloadAnnos_released(self):
        if self.pc is None or self.pc_idx is None:
            self.get_pc()
        self.reload_annotations()

app = QApplication([])
window = LidarDetWindow()
window.show()
app.exec()
