# pfsr-project

The application is a GUI tool for visualizing detections and annotations in LiDAR point clouds.
It is based on KITTI dataset.

The application was tested on Ubuntu 18.04 with Python 3.8 and Anaconda environment.

Instructions for running the app:

**1. Create conda environment:**

`conda create --name pfsr-project python=3.8`

`conda activate pfsr-project`

**2. Install dependencies:**

`pip install -r requirements.txt`

**3. Run the app:**

`python main.py`

